# -*- coding: utf-8 -*-
"""
LHM INPUT VALIDATOR

Created on Tue Oct 25 22:07:30 2022

@author: janssen_gs
"""

import imod
import xarray as xr
import numpy as np
import os
import matplotlib.pyplot as plt
from pathlib import Path

# run from basedir, assuming script resides in subdir of src/
os.chdir(r"e:\LHM_master\Data\2_Model_Input")
outpath_log = Path(r"g:\Projecten\2022\LHM_Input_Validator\data\4-output")
logf = open(outpath_log / "log.out", "w")
outpath_plots = Path(r"g:\Projecten\2022\LHM_Input_Validator\data\5-visualize")

# load LHM input
# ANI
da_ani_hoek = imod.idf.open(r"modflow\ani\hk_L*.IDF")
da_ani_factor = imod.idf.open(r"modflow\ani\fct_L*.IDF")
# BAS
da_bnd = imod.idf.open(r"modflow\bnd\ibound_l*.idf")
da_shd = imod.idf.open(r"modflow\shd\head_steady-state_l*.idf")
# BCF
da_kd = imod.idf.open(r"modflow\kd\MDL_KD_l*.idf")
da_c = imod.idf.open(r"modflow\c\MDL_C_l*.idf")
# DRN
da_cond_sof = imod.idf.open(r"modflow\drn\COND_SOF_250.IDF")
da_cond_BRP2012_MVGREP = imod.idf.open(r"modflow\drn\COND_BRP2012_MVGREP_250.IDF")
da_cond_b = imod.idf.open(r"modflow\drn\COND_B_250.IDF")
da_bodh_sof = imod.idf.open(r"modflow\drn\BODH_SOF_250.IDF")
da_bodh_BRP2012_MVGREP = imod.idf.open(r"modflow\drn\BODH_BRP2012_MVGREP_250.IDF")
da_bodh_b = imod.idf.open(r"modflow\drn\BODH_B_250.IDF")
# RIV
# -- Hoofdwatersysteem
da_riv_hws_stage = imod.idf.open(r"modflow\riv\hoofdwater\PEILH_*.idf")
da_riv_hws_cond_L1_winter = imod.idf.open(r"modflow\riv\hoofdwater\COND_HL1_250_w.idf")                                 
da_riv_hws_cond_L1_zomer = imod.idf.open(r"modflow\riv\hoofdwater\COND_HL1_250_s.idf")
da_riv_hws_cond_L2_winter = imod.idf.open(r"modflow\riv\hoofdwater\COND_HL2_250_w.idf")                                 
da_riv_hws_cond_L2_zomer = imod.idf.open(r"modflow\riv\hoofdwater\COND_HL2_250_s.idf")
da_riv_hws_bodh_L1_winter = imod.idf.open(r"modflow\riv\hoofdwater\both_w_l1.idf")                                 
da_riv_hws_bodh_L1_zomer = imod.idf.open(r"modflow\riv\hoofdwater\both_z_l1.idf")
da_riv_hws_bodh_L2_winter = imod.idf.open(r"modflow\riv\hoofdwater\both_w_l2.idf")                                 
da_riv_hws_bodh_L2_zomer = imod.idf.open(r"modflow\riv\hoofdwater\both_z_l2.idf")
# -- Regionaal systeem                                 
da_riv_primair_stage_winter = imod.idf.open(r"modflow\riv\regionaal\PEIL_P1W_250.IDF")
da_riv_primair_stage_zomer = imod.idf.open(r"modflow\riv\regionaal\PEIL_P1Z_250.IDF")                                            
da_riv_secundair_stage_winter = imod.idf.open(r"modflow\riv\regionaal\PEIL_S1W_250.IDF")
da_riv_secundair_stage_zomer = imod.idf.open(r"modflow\riv\regionaal\PEIL_S1Z_250.IDF")                                       
da_riv_tertiair_stage_winter = imod.idf.open(r"modflow\riv\regionaal\PEIL_T1W_250.IDF")
da_riv_tertiair_stage_zomer = imod.idf.open(r"modflow\riv\regionaal\PEIL_T1Z_250.IDF")                                      
da_riv_primair_cond_winter = imod.idf.open(r"modflow\riv\regionaal\cond_p_l0_w.idf")
da_riv_primair_cond_zomer = imod.idf.open(r"modflow\riv\regionaal\cond_p_l0_s.idf")
da_riv_secundair_cond_winter = imod.idf.open(r"modflow\riv\regionaal\cond_s_l0_w.idf")
da_riv_secundair_cond_zomer = imod.idf.open(r"modflow\riv\regionaal\cond_s_l0_s.idf")                                      
da_riv_tertiair_cond_winter = imod.idf.open(r"modflow\riv\regionaal\cond_t_l0_w.idf")
da_riv_tertiair_cond_zomer = imod.idf.open(r"modflow\riv\regionaal\cond_t_l0_s.idf")
da_riv_primair_bodh_winter = imod.idf.open(r"modflow\riv\regionaal\BODH_P1Z_250.IDF")
da_riv_primair_bodh_zomer = imod.idf.open(r"modflow\riv\regionaal\BODH_P1Z_250.IDF")                                  
da_riv_secundair_bodh_winter = imod.idf.open(r"modflow\riv\regionaal\BODH_S1Z_250.IDF")
da_riv_secundair_bodh_zomer = imod.idf.open(r"modflow\riv\regionaal\BODH_S1Z_250.IDF")
da_riv_tertiair_bodh = imod.idf.open(r"modflow\riv\regionaal\steady-state\BODH_T1J_250.IDF")
da_riv_primair_infmz = imod.idf.open(r"modflow\riv\regionaal\INFMZ_P1_250.IDF")
da_riv_secundair_infmz = imod.idf.open(r"modflow\riv\regionaal\INFMZ_S1_250.IDF")
da_riv_tertiair_infmz = imod.idf.open(r"modflow\riv\regionaal\INFMZ_T1_250.IDF")
da_riv_primair_infmz = imod.idf.open(r"modflow\riv\regionaal\INFMF_P1_250.IDF")
da_riv_secundair_infmf = imod.idf.open(r"modflow\riv\regionaal\INFMF_S1_250.IDF")
da_riv_tertiair_infmf = imod.idf.open(r"modflow\riv\regionaal\INFMF_T1_250.IDF")
# -- Wells 
da_riv_well_stage_winter = imod.idf.open(r"modflow\riv\wellen\peilw_wel.idf")
da_riv_well_stage_zomer = imod.idf.open(r"modflow\riv\wellen\peilz_wel.idf")                                       
da_riv_well_bodh = imod.idf.open(r"modflow\riv\wellen\bodh_wel.idf")                         
da_riv_well_cond = imod.idf.open(r"modflow\riv\wellen\cond_wel.IDF")
da_riv_well_inff = imod.idf.open(r"modflow\riv\wellen\inff_wel.idf")
# DIS
da_top = imod.idf.open(r"modflow\top\MDL_TOP_l*.idf")
da_bot = imod.idf.open(r"modflow\bot\MDL_BOT_l*.idf")
da_thk_aqf = da_top-da_bot #aquifer thickness
np_thk_aqt = np.zeros((da_thk_aqf.shape[0]-1,da_thk_aqf.shape[1],da_thk_aqf.shape[2])) #aquitard thickness
for i in range(0,len(np_thk_aqt)):
    np_thk_aqt[i] = da_bot[i].values - da_top[i+1].values
da_thk_aqt = xr.DataArray(np_thk_aqt, coords=da_thk_aqf[:len(np_thk_aqt)].coords, dims=da_thk_aqf.dims)    
# STO
da_sto = imod.idf.open(r"g:\Projecten\2022\LHM_4_3\sto\sto_l*.idf") #already updated, currently in temp folder
# GHB
da_ghb_head = imod.idf.open(r"modflow\ghb\ghb_head_l1.idf")
da_ghb_cond = imod.idf.open(r"modflow\ghb\ghb_cond_l1.idf")

print("Succesfully imported LHM input")


#Tests 2: BCF
logf.writelines('***********************************************' + '\n')
logf.writelines('BCF' + '\n')
logf.writelines('***********************************************' + '\n')

#2.1 Aquitards zonder weerstand
da_inc = xr.where((da_thk_aqt > 0.1)&(da_c < 2.0),da_thk_aqt,0)
da = da_inc.where(da_inc > 0.)
for lay in range(0, 7):
    if da.count(dim=['y','x']).values[lay] > 0:
        logf.writelines('Laag ' + str(lay+1) + ': aantal cellen met aquitard zonder weerstand: ' + str(da.count(dim=['y','x']).values[lay]) + '\n')
        da.isel(layer = lay).plot(cmap = 'RdYlBu_r', figsize = (8, 5), vmin = 0, vmax = 10)
        plt.title(r'Aquitarddikte zonder weerstand (m) ' + 'Laag ' + str(lay+1))
        fname =  "Aquitarddikte_zonder_weerstand_L" + str(lay+1) + ".png"
        plt.savefig(outpath_plots / fname)
        plt.close()
        
#2.2 Weerstand zonder aquitard
da_inc = xr.where((da_thk_aqt < 0.1)&(da_c > 2.0),da_c,0)
da = da_inc.where(da_inc > 0.)
for lay in range(0, 7):
    if da.count(dim=['y','x']).values[lay] > 0:
        logf.writelines('Laag ' + str(lay+1) + ': aantal cellen met weerstand zonder aquitarddikte: ' + str(da.count(dim=['y','x']).values[lay]) + '\n')
        da.isel(layer = lay).plot(cmap = 'RdYlBu_r', figsize = (8, 5), vmin = 2., vmax = 20000)
        plt.title(r'Weerstand zonder aquitarddikte (d) ' + 'Laag ' + str(lay+1))
        fname =  "Weerstand_zonder_aquitarddikte_L" + str(lay+1) + ".png"
        plt.savefig(outpath_plots / fname)
        plt.close()

#2.3 Specific storage tussen 1E4 en 1E6
#In nieuwe opzet (en i.t.t. LHM 4.2) betreffen de STO-invoer-IDF's direct de bergingscoefficienten,
#oftewel specific storage maal modellaagdikte. Hier wordt gecheckt of, terugrekenend met de modellaagdikte,
#de specific storage gangbare waarden heeft.
da_ss = xr.where(da_thk_aqf.isel(layer=lay) > 0, da_sto/da_thk_aqf.isel(layer=lay), 0.)
da_ss = da_ss.where(da_ss > 0.)
da_logss = xr.DataArray(np.log10(da_ss.values),coords=da_ss.coords, dims=da_ss.dims) 
da_logss_out_of_range = da_logss.where((da_logss < 4.) | (da_logss > 6.))
for lay in range(1, 8):
    if da_logss_out_of_range.count(dim=['y','x']).values[lay] > 0:
        logf.writelines('Laag ' + str(lay+1) + ': aantal cellen met extreme specific storage: ' + str(da_logss_out_of_range.count(dim=['y','x']).values[lay]) + '\n')
        da_logss_out_of_range.isel(layer = lay).plot(cmap = 'RdYlBu_r', figsize = (8, 5), vmin = 0, vmax = 10)
        plt.title(r'LOG Specific Storage (-) ' + 'Laag ' + str(lay+1))
        fname =  "LOG_specific_storage_L" + str(lay+1) + ".png"
        plt.savefig(outpath_plots / fname)
        plt.close()
 
#2.4 K-waarden
#Check of KH-waarden < 100 resp. 1000 dagen.
da_kh = xr.where((da_kd > 0.)&(da_thk_aqf > 0.), da_kd/da_thk_aqf, 0.)
da_kh_gt_100 = da_kh.where(da_kh > 100.)
da_kh_gt_1000 = da_kh.where(da_kh > 1000.)
for lay in range(0, 8):
    if da_kh_gt_100.count(dim=['y','x']).values[lay] > 0:
        logf.writelines('Laag ' + str(lay+1) + ': aantal cellen met Kh > 100 m/d: ' + str(da_kh_gt_100.count(dim=['y','x']).values[lay]) + '\n')
        da_kh_gt_100.isel(layer = lay).plot(cmap = 'RdYlBu_r', figsize = (8, 5), vmin = 100, vmax = 1000)
        plt.title(r'KH (m/d) ' + 'Laag ' + str(lay+1))
        fname =  "KH_GT_100_L" + str(lay+1) + ".png"
        plt.savefig(outpath_plots / fname)
        plt.close()
    if da_kh_gt_1000.count(dim=['y','x']).values[lay] > 0:
        logf.writelines('Laag ' + str(lay+1) + ': aantal cellen met Kh > 1000 m/d: ' + str(da_kh_gt_1000.count(dim=['y','x']).values[lay]) + '\n')
        da_kh_gt_1000.isel(layer = lay).plot(cmap = 'RdYlBu_r', figsize = (8, 5), vmin = 1000, vmax = 10000)
        plt.title(r'KH (m/d) ' + 'Laag ' + str(lay+1))
        fname =  "KH_GT_1000_L" + str(lay+1) + ".png"
        plt.savefig(outpath_plots / fname)
        plt.close()    

logf.writelines('\n')

#2.5 C-waarden
#Check of C-waarden groter zijn dan 1 dag en kleiner dan 500000 resp. 10000000 dagen.
da_c_lt_1 = da_c.where(da_c < 1.0)
da_c_gt_500000 = da_c.where(da_c > 500000.)
da_c_gt_10000000 = da_c.where(da_c > 10000000.)
for lay in range(0, 7):
    if da_c_lt_1.count(dim=['y','x']).values[lay] > 0:
        logf.writelines('Laag ' + str(lay+1) + ': aantal cellen met C < 1 d: ' + str(da_c_lt_1.count(dim=['y','x']).values[lay]) + '\n')
        da_c_lt_1.isel(layer = lay).plot(cmap = 'RdYlBu_r', figsize = (8, 5), vmin = 0., vmax = 1.)
        plt.title(r'C (d) ' + 'Laag ' + str(lay+1))
        fname =  "C_LT_1_L" + str(lay+1) + ".png"
        plt.savefig(outpath_plots / fname)
        plt.close()
    if da_c_gt_500000.count(dim=['y','x']).values[lay] > 0:
        logf.writelines('Laag ' + str(lay+1) + ': aantal cellen met C > 500000 d: ' + str(da_c_gt_500000.count(dim=['y','x']).values[lay]) + '\n')
        da_c_gt_500000.isel(layer = lay).plot(cmap = 'RdYlBu_r', figsize = (8, 5), vmin = 500000., vmax = 10000000.)
        plt.title(r'C (d) ' + 'Laag ' + str(lay+1))
        fname =  "C_GT_500000_L" + str(lay+1) + ".png"
        plt.savefig(outpath_plots / fname)
        plt.close()
    if da_c_gt_10000000.count(dim=['y','x']).values[lay] > 0:
        logf.writelines('Laag ' + str(lay+1) + ': aantal cellen met C > 10000000 d: ' + str(da_c_gt_10000000.count(dim=['y','x']).values[lay]) + '\n')
        da_c_gt_10000000.isel(layer = lay).plot(cmap = 'RdYlBu_r', figsize = (8, 5), vmin = 10000000., vmax = 100000000.)
        plt.title(r'C (d) ' + 'Laag ' + str(lay+1))
        fname =  "C_GT_10000000_L" + str(lay+1) + ".png"
        plt.savefig(outpath_plots / fname)
        plt.close()            

#7 ANI
#7.1 Check of anisotropiefactor altijd < 1.
da_inc = da_ani_factor.where(da_ani_factor > 1.)
for i in range(0, da_ani_factor.shape[0]):
    lay = da_ani_factor.layer[i]
    if da_inc.count(dim=['y','x']).values[i] > 0:
        da_inc.isel(layer = i).plot(cmap = 'RdYlBu_r', figsize = (8, 5), vmin = 1., vmax = 100.)
        plt.title(r'Anisotropiefactor (-) ' + 'Laag ' + str(lay))
        fname =  "Anisotropiefactor_GT_1_L" + str(lay) + ".png"
        plt.savefig(outpath_plots / fname)
        plt.close()        
#7.2 Check of anisotropiehoek ligt tussen 0 en 180 (inclusief)
da_inc = da_ani_hoek.where((da_ani_hoek < 0.) | (da_ani_hoek > 180.))
for i in range(0, da_ani_factor.shape[0]):
    lay = da_ani_hoek.layer[i]
    if da_inc.count(dim=['y','x']).values[i] > 0:
        da_inc.isel(layer = i).plot(cmap = 'RdYlBu_r', figsize = (8, 5), vmin = -360., vmax = 360.)
        plt.title(r'Anisotropiehoek (graden) ' + 'Laag ' + str(lay))
        fname =  "Anisotropiehoek_LT_0_of_GT_180_L" + str(lay) + ".png"
        plt.savefig(outpath_plots / fname)
        plt.close()              
        
logf.close()


